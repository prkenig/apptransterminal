# TransTerminal
The application of the logistics company TRANS Terminal. The doc API is used for work https://tt-express.ru/doc_api
 
## Installation

Technology used react-native.
 - See https://facebook.github.io/react-native/docs/getting-started;

Commands to execute after installing react-native:
```
git clone git@bitbucket.org:prkenig/apptransterminal.git TransTerminal
npm i
yarn install
npm install -g react-native-cli
add android/app/debug.keystore
react-native run-android
```
