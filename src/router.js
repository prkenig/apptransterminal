import {createStackNavigator, createSwitchNavigator} from 'react-navigation';
import React from 'react';
import HomeScreen from "./screens/HomeScreen";
import MigrationScreen from "./screens/MigrationScreen";
import AuthLoadingScreen from "./screens/AuthLoadingScreen";
import SignInScreen from "./screens/SignInScreen";

export const NavStack = createStackNavigator(
        {
        HomeScreen: {
            screen: HomeScreen,
            navigationOptions: {
                header: null
            }
        },
        MigrationScreen: {
            screen: MigrationScreen,
        },
    },
    {
        initialRouteName: 'HomeScreen'
    }
);

export const AuthStack = createStackNavigator(
    {
        SignInScreen: {
            screen: SignInScreen,
            navigationOptions: {
                header: null
            }
        },
    }
);

export const switchNavigator = createSwitchNavigator( {
    AuthLoading: AuthLoadingScreen,
    App: NavStack,
    Auth: AuthStack,
},
{
    initialRouteName: 'AuthLoading',
});
