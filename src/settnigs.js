import AsyncStorage from '@react-native-community/async-storage';
import React from 'react';

export const START_SETTINGS = "settings";

let defaultSettings = {
    language: null,
    token: null
};

export let settings;

export const setSettings = async (value) => {
    settings = Object.assign({}, settings, value);

    return new Promise((resolve, reject) => {
        AsyncStorage.setItem(START_SETTINGS, JSON.stringify(settings))
            .then(() => {
                resolve(true);
            })
            .catch(err => reject(err));
    });
};

export const getSettings = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(START_SETTINGS)
            .then(res => {
                if (res === null || res === 'true' ) {
                    settings = defaultSettings;
                    resolve(settings);
                } else {
                    settings = JSON.parse(res);
                    resolve(settings);
                }
            })
            .catch(err => reject(err));
    });
};
