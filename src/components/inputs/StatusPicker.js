import React from "react";
import {Container, Header, Content, Icon, Picker, Form, H3, ListItem, Left, Text, Body} from 'native-base';
import SpinnerTheme from '../SpinnerTheme';
import StatusForm from '../forms/StatusForm';
import t from '../../language/Collections';
import TTExpressRestApi from '../../client/TTExpressRestApi';
import {settings} from '../../settnigs';

export default class StatusPicker extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: undefined,
            items: undefined
        };
    }

    onValueChange(value: string) {
        this.setState({
            selected: value
        });
        this.props.onValueChange(value);
    }

    _load(){
        const api = new TTExpressRestApi(settings.token);

        api.statusCollections()
            .then(data => {
                if(data.status === 'success'){
                    let items = Object.entries(data.response.permissions);
                    this.setState({items: items});
                }
            })
            .catch(err => console.error(err));
    }

    componentWillMount() {
        this._load();
    }

    render() {
        return (this.state.items === undefined
                ? <SpinnerTheme/>
                : <Picker
                    mode="dropdown"
                    selectedValue={this.state.selected}
                    onValueChange={this.onValueChange.bind(this)}
                >
                    <Picker.Item label={t.select_status} value={null} />
                    {
                        this.state.items.map((item, index) => {
                            return (<Picker.Item key={index} label={item[1]} value={item[0]} />)
                        })
                    }
                </Picker>
        );
    }
}
