import React from "react";
import {Button, View, Content, Icon, Picker, Form, H3, ListItem, Left, Text, Body, Container} from 'native-base';
import {TouchableHighlight} from 'react-native';
import SpinnerTheme from '../SpinnerTheme';
import StatusForm from '../forms/StatusForm';
import t from '../../language/Collections';
import TTExpressRestApi from '../../client/TTExpressRestApi';
import {settings} from '../../settnigs';
import SearchContent from '../SearchContent';
import Modal from "react-native-modal";
import styles, {color_theme} from '../../styles/Base.styles';
import SignatureCapture from 'react-native-signature-capture';
import {KEY_WEIGHING} from '../../models/Parcel';

export default class SignaturePicker extends React.Component {

    state = {
        isModalVisible: false
    };

    constructor(props){
        super(props);
        this.saveSign = this.saveSign.bind(this);
        this._onSaveEvent = this._onSaveEvent.bind(this);
    }

    openModal = () => {
        this.setState({isModalVisible: true});
    };

    closeModal = () => {
        this.setState({isModalVisible: false});
    };

    saveSign() {
        this.refs["sign"].saveImage();
    }

    _onSaveEvent(result) {
        let image = 'data:image/png;base64,' + result.encoded;
        this.closeModal();
        return this.props.onChange(image);
    }

    render() {
        return (
            <>
                <Button onPress={this.openModal} style={styles.btnThemeBorder}>
                    <Text style={{textAlign: 'center', width: '100%', color:color_theme}}>{'Поставить подпись'}</Text>
                </Button>
                <Modal
                    isVisible={this.state.isModalVisible}
                    onBackdropPress={this.closeModal}
                >
                    <Container style={{backgroundColor: 'white'}}>
                        <SignatureCapture
                            style={{ flex:1}}
                            ref="sign"
                            onSaveEvent={this._onSaveEvent}
                            saveImageFileInExtStorage={false}
                            showNativeButtons={false}
                            showTitleLabel={false}
                            viewMode={"portrait"}/>

                        <View style={styles.footerSectionBtns}>
                            <Button full onPress={this.closeModal} style={{width: '50%', backgroundColor:color_theme}}>
                                <Text>{t.close}</Text>
                            </Button>
                            <Button full onPress={this.saveSign} style={[{width: '50%'}, styles.btnThemeBorder]}>
                                <Text style={{color:color_theme}}>{t.save}</Text>
                            </Button>
                        </View>
                    </Container>
                </Modal>
            </>
        );
    }
}
