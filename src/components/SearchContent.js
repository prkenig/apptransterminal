import React from 'react';
import {Button, Container, Content, Icon, Text, List, ListItem, H3} from 'native-base';
import StatusForm from './forms/StatusForm';
import SpinnerTheme from './SpinnerTheme';
import TTExpressRestApi from '../client/TTExpressRestApi';
import t from '../language/Collections';
import {color_theme} from '../styles/Base.styles';
import {settings} from '../settnigs';
import OrderView from './OrderView';
import ParcelSortView from './ParcelSortView';
import ParcelView from './ParcelView';

const TYPE_PARCEL = 'parcel';
const TYPE_ORDER = 'order';
const TYPE_PARCEL_SORT = 'parcel_sort';

export default class SearchContent extends React.Component {

    type = TYPE_PARCEL;

    constructor(props){
        super(props);

        this.state = {
            response: null,
            loaded: null,
            number: props.number,
        };

        this._response = this._response.bind(this);
    }

    changeNumber = (number) => {
        this.setState({loaded: null});
        this._load(number);
    };

    _load(number){
        this._type_detecte(number);

        const api = new TTExpressRestApi(settings.token);

        switch (this.type) {
            case TYPE_ORDER:
                api.ordersView(number).then(this._response).catch(err => console.error(err));
                break;
            case TYPE_PARCEL_SORT:
                api.parcelSortsView(number).then(this._response).catch(err => console.error(err));
                break;
            case TYPE_PARCEL:
                api.parcelsView(number).then(this._response).catch(err => console.error(err));
                break;
        }
    }

    _response(data){
        if(data.status === 'success'){
            this.setState({response: data.response});
            this.setState({loaded: true});
        } else {
            this.setState({loaded: false});
        }
    }

    _type_detecte(number){
        if(/^TT[\d]{1}/gi.test(number)){
            this.type = TYPE_ORDER;
        } else if(/^TTSORT/gi.test(number)){
            this.type = TYPE_PARCEL_SORT;
        } else {
            this.type = TYPE_PARCEL;
        }
    }

    componentWillMount() {
        this._load(this.state.number);
    }

    render() {

        let ViewItem = React.Fragment;

        switch (this.type) {
            case TYPE_ORDER:
                ViewItem = OrderView;
                break;
            case TYPE_PARCEL_SORT:
                ViewItem = ParcelSortView;
                break;
            case TYPE_PARCEL:
                ViewItem = ParcelView;
                break;
        }

        return ( this.state.loaded === null
                    ? <SpinnerTheme/>
                    : this.state.loaded
                        ? <ViewItem
                            response={this.state.response}
                            closeModal={this.props.closeModal}
                            changeNumber={this.changeNumber}
                            parent={this}
                        />
                        :<>
                            <Content>
                                <H3 style={{textAlign: 'center', marginTop:30}}>{t.not_found}</H3>
                            </Content>
                            <Button full onPress={this.props.closeModal} style={{backgroundColor:color_theme}}>
                                <Text>{t.close}</Text>
                            </Button>
                        </>
            );
    }
}
