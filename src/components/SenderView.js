import React from 'react';
import {List, ListItem, Text, Left, Body, Separator, Content} from 'native-base';
import styles from '../styles/Base.styles';
import t from '../language/Collections';
import Sender from '../models/Sender';
import ItemInfo from './ItemInfo';
import PropTypes from 'prop-types';
import {Linking} from 'react-native';

const SenderView = (props) => {
    return (
        <>
            <Separator>
                <Text style={styles.separatorText}>{t.sender}</Text>
            </Separator>

            <ItemInfo
                label={props.model.attributes.name.label}
                value={props.model.data[props.model.attributes.name.name]}
            />

            <ItemInfo
                label={props.model.attributes.phone.label}
                onPress={()=>{
                    Linking.openURL(`tel:${props.model.data[props.model.attributes.phone.name]}`)
                }}
                value={props.model.data[props.model.attributes.phone.name]}
            />
        </>
    );
};

SenderView.propTypes = {
    model: PropTypes.instanceOf(Sender),
};

export default SenderView;
