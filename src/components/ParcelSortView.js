import React, {Component} from 'react';
import {Alert, View} from 'react-native';
import {Button, Content, Text, Separator} from 'native-base';
import StatusPicker from './inputs/StatusPicker';
import t from '../language/Collections';
import styles, {color_theme} from '../styles/Base.styles';
import PropTypes from 'prop-types';
import ParcelSort from '../models/ParcelSort';
import ItemInfo from './ItemInfo';
import StatusErrors from './StatusErrors';
import TTExpressRestApi from '../client/TTExpressRestApi';
import {settings} from '../settnigs';

export default class ParcelSortView extends Component {

    static propTypes = {
        response: PropTypes.object,
        closeModal: PropTypes.func,
        changeNumber: PropTypes.func,
    };

    state = {
        statusErrors: null
    };

    /**
     * @type {ParcelSort}
     */
    parcelSort;

    status_colections_id = null;

    changeStatus = (value) => {
        this.status_colections_id = value;
    };

    setStatus = () => {
        if(this.status_colections_id === null) {
            return Alert.alert(t.status_not_selected);
        }
        const code = this.parcelSort.data[this.parcelSort.attributes.code.name];
        const api = new TTExpressRestApi(settings.token);

        api.parcelSortSetStatus(code, this.status_colections_id)
            .then(data => {
                if(data.status === 'success'){
                    Alert.alert(data.response);
                    this.props.changeNumber(code);
                } else {
                    this.setState({
                        statusErrors: data.response
                    });
                }
            })
            .catch(err => console.error(err));
    };

    printSticker = () => {
        const code = this.parcelSort.data[this.parcelSort.attributes.code.name];
        const api = new TTExpressRestApi(settings.token);
        api.parcelSortsSticker(code).catch(error => console.error(error));
    };

    componentWillMount() {
        this.parcelSort = new ParcelSort();
        this.parcelSort.setData(this.props.response);
    }

    render() {
        return (
            <>
                <Content style={{marginBottom: 50}}>
                    <Separator bordered>
                        <Text style={{fontSize: 25, textAlign: 'center'}}>{this.parcelSort.data[this.parcelSort.attributes.code.name]}</Text>
                    </Separator>

                    <ItemInfo
                        label={this.parcelSort.attributes.name.label}
                        value={this.parcelSort.data[this.parcelSort.attributes.name.name]}
                    />

                    <Separator>
                        <Text style={styles.separatorText}>{t.actions}</Text>
                    </Separator>

                    {this.state.statusErrors !== null && <StatusErrors
                        errors={this.state.statusErrors}
                    />}

                    <StatusPicker onValueChange={this.changeStatus}/>

                    <Button onPress={this.printSticker} style={styles.btnThemeBorder}>
                        <Text style={{textAlign: 'center', width: '100%', color:color_theme}}>{t.print_sticker}</Text>
                    </Button>

                </Content>

                <View style={styles.footerSectionBtns}>
                    <Button full onPress={this.props.closeModal} style={{width: '50%', backgroundColor:color_theme}}>
                        <Text>{t.close}</Text>
                    </Button>
                    <Button full onPress={this.setStatus} style={[{width: '50%'}, styles.btnThemeBorder]}>
                        <Text style={{color:color_theme}}>{t.apply}</Text>
                    </Button>
                </View>
            </>
        );
    }
};
