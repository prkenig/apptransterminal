import React, {Component} from 'react';
import {
    Item as FormItem,
    Label,
    Input,
    Content
} from 'native-base';
import Parcel from '../../models/Parcel';
import PropTypes from 'prop-types';

export default class WeightForm extends Component {

    static propTypes = {
        parcel: PropTypes.instanceOf(Parcel),
        onChange: PropTypes.func,
    };

    constructor(props){
        super(props);

        this.package_weight = props.parcel.data[props.parcel.attributes.package_weight.name];
        this.package_height = props.parcel.data[props.parcel.attributes.package_height.name];
        this.package_length = props.parcel.data[props.parcel.attributes.package_length.name];
        this.package_width = props.parcel.data[props.parcel.attributes.package_width.name];
    }

    onChange(){
        let values = {
            package_weight: this.package_weight,
            package_height: this.package_height,
            package_length: this.package_length,
            package_width: this.package_width,
        };
        return this.props.onChange(values);
    }

    render() {
        const parcel = this.props.parcel;

        return (
            <Content padder>
                <FormItem stackedLabe error = {false}>
                    <Label style={{fontSize:12}}>{parcel.attributes.package_weight.label}</Label>
                    <Input
                        keyboardType={'decimal-pad'}
                        autoCorrect={false}
                        defaultValue={this.package_weight}
                        onChangeText={(value)=>{
                            this.package_weight = value;
                            this.onChange();
                        }}
                    />
                </FormItem>
                <FormItem stackedLabe error = {false}>
                    <Label style={{fontSize:12}}>{parcel.attributes.package_height.label}</Label>
                    <Input
                        keyboardType={'decimal-pad'}
                        autoCorrect={false}
                        defaultValue={this.package_height}
                        onChangeText={(value)=>{
                            this.package_height = value;
                            this.onChange();
                        }}
                    />
                </FormItem>
                <FormItem stackedLabe error = {false}>
                    <Label style={{fontSize:12}}>{parcel.attributes.package_length.label}</Label>
                    <Input
                        keyboardType={'decimal-pad'}
                        autoCorrect={false}
                        defaultValue={this.package_length}
                        onChangeText={(value)=>{
                            this.package_length = value;
                            this.onChange();
                        }}
                    />
                </FormItem>
                <FormItem stackedLabe error = {false}>
                    <Label style={{fontSize:12}}>{parcel.attributes.package_width.label}</Label>
                    <Input
                        keyboardType={'decimal-pad'}
                        autoCorrect={false}
                        defaultValue={this.package_width}
                        onChangeText={(value)=>{
                            this.package_width = value;
                            this.onChange();
                        }}
                    />
                </FormItem>
            </Content>
        );
    }
};
