import React, {Component} from 'react';
import {View} from 'react-native';
import {Button, Container, Content, Icon, Text, ListItem, H3, Separator} from 'native-base';
import ListStatuses from '../ListStatuses';
import StatusPicker from '../inputs/StatusPicker';
import t from '../../language/Collections';
import styles, {color_theme} from '../../styles/Base.styles';

export default class StatusForm extends Component {

    changeStatus = (value) => {
        console.log('changeStatus', value);
    };

    render() {

        return (
            <>
                <Content>
                    <Separator bordered>
                        <Text style={{fontSize: 25, textAlign: 'center'}}>{this.props.number}</Text>
                    </Separator>

                    <ListStatuses statuses={this.props.statuses}/>

                    <Separator>
                        <Text style={styles.separatorText}>{t.actions}</Text>
                    </Separator>

                    <StatusPicker onValueChange={this.changeStatus}/>

                </Content>

                <View style={{flex:1, flexDirection: 'row', position: 'absolute', bottom:0, justifyContent: 'flex-end',}}>
                    <Button full onPress={this.props.closeModal} style={{width: '50%', backgroundColor:color_theme}}>
                        <Text>{t.close}</Text>
                    </Button>
                    <Button full onPress={()=>{}} style={{width: '50%', backgroundColor:'white', borderWidth:1, borderColor: color_theme}}>
                        <Text style={{color:color_theme}}>{t.apply}</Text>
                    </Button>
                </View>
            </>
        );
    }
};
