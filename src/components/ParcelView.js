import React, {Component} from 'react';
import {View, Linking, Alert, Dimensions} from 'react-native';
import {Button, Container, Content, Item, Text, Label, Input, Separator, ListItem, Left} from 'native-base';
import ListStatuses from './ListStatuses';
import ListCorporationStatuses from './ListCorporationStatuses';
import StatusPicker from './inputs/StatusPicker';
import ItemInfo from './ItemInfo';
import t from '../language/Collections';
import Parcel, {KEY_SIGNATURE, KEY_WEIGHING} from '../models/Parcel';
import StatusErrors from './StatusErrors';
import styles, {color_theme} from '../styles/Base.styles';
 import PropTypes from 'prop-types';
import TTExpressRestApi from '../client/TTExpressRestApi';
import {settings} from '../settnigs';
import WeightForm from './forms/WeightForm';
import SignaturePicker from './inputs/SignaturePicker';
import AutoHeightImage from 'react-native-auto-height-image';

const { width } = Dimensions.get('window');

export default class ParcelView extends Component {

    static propTypes = {
        response: PropTypes.object,
        closeModal: PropTypes.func,
        changeNumber: PropTypes.func,
    };

    state = {
        statusErrors: null
    };

    /**
     * @type {Parcel}
     */
    parcel;

    data_package = {
        package_weight: null,
        package_height: null,
        package_length: null,
        package_width: null,
    };
    status_colections_id = null;

    changeStatus = (value) => {
        this.status_colections_id = value;
    };

    changeSignature = (value) => {
        const tracking_number = this.parcel.getTrackingNumber();
        const api = new TTExpressRestApi(settings.token);

        api.parcelsSetSignature(tracking_number, value)
            .then(data => {
                if(data.status === 'success'){
                    Alert.alert(data.response);
                    this.props.changeNumber(tracking_number);
                } else {
                    let errors = {};
                    errors[tracking_number] = data.response;

                    this.setState({
                        statusErrors: errors
                    });
                }
            })
            .catch(err => console.error(err));
    };

    setStatus = () => {
        if(this.status_colections_id === null) {
            return Alert.alert(t.status_not_selected);
        }
        const tracking_number = this.parcel.getTrackingNumber();
        const api = new TTExpressRestApi(settings.token);

        api.parcelsSetStatus(tracking_number, this.status_colections_id)
            .then(data => {
                if(data.status === 'success'){
                    Alert.alert(data.response);
                    this.props.changeNumber(tracking_number);
                } else {
                    let errors = {};
                    errors[tracking_number] = data.response;

                    this.setState({
                        statusErrors: errors
                    });
                }
            })
            .catch(err => console.error(err));
    };

    printSticker = () => {
        const tracking_number = this.parcel.getTrackingNumber();
        const api = new TTExpressRestApi(settings.token);
        api.parcelsSticker(tracking_number).catch(error => console.error(error));
    };

    setWeight = () => {
        if(this.data_package.package_weight === null) {
            return Alert.alert(t.weight_not_set);
        }
        const tracking_number = this.parcel.getTrackingNumber();
        const api = new TTExpressRestApi(settings.token);

        api.parcelsSetWeight(tracking_number, this.data_package)
            .then(data => {
                if(data.status === 'success'){
                    Alert.alert(data.response);
                    this.props.changeNumber(tracking_number);
                } else {
                    let errors = {};
                    errors[tracking_number] = data.response;

                    this.setState({
                        statusErrors: errors
                    });
                }
            })
            .catch(err => console.error(err));
    };

    changeWeight = (data_package) => {
      this.data_package = data_package;
    };

    componentWillMount() {
        this.parcel = new Parcel();
        this.parcel.setData(this.props.response);
    }

    render() {

        const key = this.parcel.getCurrentKey();

        return (
            <>
                <Content style={{marginBottom: 50}}>
                    <Separator bordered>
                        <Text style={{fontSize: 25, textAlign: 'center'}}>{this.parcel.getTrackingNumber()}</Text>
                    </Separator>

                    <ItemInfo
                        label={this.parcel.attributes.order_number.label}
                        value={this.parcel.data[this.parcel.attributes.order_number.name]}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.full_name.label}
                        value={this.parcel.data[this.parcel.attributes.full_name.name]}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.email.label}
                        onPress={()=>{
                            Linking.openURL(`mailto:${this.parcel.data[this.parcel.attributes.email.name]}`)
                        }}
                        value={this.parcel.data[this.parcel.attributes.email.name]}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.phone.label}
                        onPress={()=>{
                            Linking.openURL(`tel:${this.parcel.data[this.parcel.attributes.phone.name]}`)
                        }}
                        value={this.parcel.data[this.parcel.attributes.phone.name]}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.address.label}
                        value={this.parcel.addressFormat()}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.description.label}
                        value={this.parcel.data[this.parcel.attributes.description.name]}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.unit_cost.label}
                        value={this.parcel.data[this.parcel.attributes.unit_cost.name]}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.unit_weight.label}
                        value={this.parcel.data[this.parcel.attributes.unit_weight.name]}
                    />

                    <Separator>
                        <Text style={styles.separatorText}>{t.package}</Text>
                    </Separator>

                    <ItemInfo
                        label={this.parcel.attributes.package_weight.label}
                        value={this.parcel.data[this.parcel.attributes.package_weight.name]}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.package_height.label}
                        value={this.parcel.data[this.parcel.attributes.package_height.name]}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.package_length.label}
                        value={this.parcel.data[this.parcel.attributes.package_length.name]}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.package_width.label}
                        value={this.parcel.data[this.parcel.attributes.package_width.name]}
                    />

                    <Separator>
                        <Text style={styles.separatorText}>{t.sorting}</Text>
                    </Separator>

                    <ItemInfo
                        label={this.parcel.attributes.invoice_number.label}
                        onPress={()=>{
                            this.props.changeNumber(this.parcel.data[this.parcel.attributes.invoice_number.name]);
                        }}
                        value={this.parcel.data[this.parcel.attributes.invoice_number.name]}
                    />

                    <ItemInfo
                        label={this.parcel.attributes.sticker_consolidating.label}
                        value={this.parcel.data[this.parcel.attributes.sticker_consolidating.name]}
                    />

                    {this.parcel.data[this.parcel.attributes.parcel_sorts.name].map((sort, index) => {
                        return (
                            <ItemInfo
                                key={index}
                                onPress={()=>{this.props.changeNumber(sort.code);}}
                                label={sort.name}
                                value={sort.code}
                            />
                        )
                    })}

                    <Separator>
                        <Text style={styles.separatorText}>{t.status}</Text>
                    </Separator>

                    <ListCorporationStatuses statuses={this.parcel.data[this.parcel.attributes.status.name]}/>

                    <Separator>
                        <Text style={styles.separatorText}>{t.actions}</Text>
                    </Separator>

                    {this.state.statusErrors !== null && <StatusErrors
                        errors={this.state.statusErrors}
                    />}

                    {key === KEY_WEIGHING && this.parcel.isEmptyWeight() ?
                        <WeightForm
                            parcel={this.parcel}
                            onChange={this.changeWeight}
                        /> :
                        <StatusPicker onValueChange={this.changeStatus}/>
                    }

                    {(key === KEY_WEIGHING && !this.parcel.isEmptyWeight()) &&
                        <Button onPress={this.printSticker} style={styles.btnThemeBorder}>
                            <Text style={{textAlign: 'center', width: '100%', color:color_theme}}>{t.print_sticker}</Text>
                        </Button>
                    }

                    {(key === KEY_SIGNATURE && this.parcel.isEmptySignature()) &&
                        <SignaturePicker onChange={this.changeSignature}/>
                    }

                    {!this.parcel.isEmptySignature() &&
                        <View style={{justifyContent:'center', padding: 10, width:'100%'}}>
                            <AutoHeightImage
                                width={width-50}
                                source={{uri:this.parcel.data[this.parcel.attributes.signature.name]}}
                            />
                        </View>
                    }

                </Content>

                <View style={styles.footerSectionBtns}>
                    <Button full onPress={this.props.closeModal} style={{width: '50%', backgroundColor:color_theme}}>
                        <Text>{t.close}</Text>
                    </Button>
                    {key === KEY_WEIGHING && this.parcel.isEmptyWeight() ?
                        <Button full onPress={this.setWeight} style={[{width: '50%'}, styles.btnThemeBorder]}>
                            <Text style={{color:color_theme}}>{t.set_weight}</Text>
                        </Button> :
                        <Button full onPress={this.setStatus} style={[{width: '50%'}, styles.btnThemeBorder]}>
                            <Text style={{color:color_theme}}>{t.apply}</Text>
                        </Button>
                    }
                </View>
            </>
        );
    }
};
