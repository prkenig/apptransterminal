import React, {Component} from 'react';
import {ActionSheet, Text} from "native-base";
import t from "../../language/Collections";
import { setSettings} from "../../settnigs";
import {goToHome } from "../../navigations";

export default class LanguageButton extends Component{

    language = {
        en: t.english,
        ru: t.russian,
        ch: t.chinese,
    };

    constructor(props) {
        super(props);
        this.state = {
            languageText: this.language[t.getLanguage()]
        };
    }

    onSelect(index){
        let value = Object.entries(this.language)[index][0];

        this.setState({languageText: this.language[value]});

        t.setLanguage(value);

        setSettings({language: value}).then(res => {
            goToHome();
        }).catch(error => console.error(error));
    }

    render() {
        const buttons = Object.values(this.language);

        return <Text onPress={() =>
            ActionSheet.show(
                {
                    options: buttons,
                    title: t.selectLanguage
                },
                this.onSelect.bind(this)
            )}>{this.state.languageText}</Text>;
    }
}
