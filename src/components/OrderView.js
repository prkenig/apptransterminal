import React, {Component} from 'react';
import {Alert, Linking, View} from 'react-native';
import {Button, Container, Content, Icon, Text, ListItem, H3, Separator} from 'native-base';
import StatusPicker from './inputs/StatusPicker';
import t from '../language/Collections';
import styles, {color_theme} from '../styles/Base.styles';
import PropTypes from 'prop-types';
import Order from '../models/Order';
import CarrierView from './CarrierView';
import PersonView from './PersonView';
import SenderView from './SenderView';
import CustomsPostView from './CustomsPostView';
import ItemInfo from './ItemInfo';
import ListCorporationStatuses from './ListCorporationStatuses';
import StatusErrors from './StatusErrors';
import TTExpressRestApi from '../client/TTExpressRestApi';
import {settings} from '../settnigs';

export default class OrderView extends Component {

    static propTypes = {
        response: PropTypes.object,
        closeModal: PropTypes.func,
        changeNumber: PropTypes.func,
    };

    state = {
        statusErrors: null
    };

    /**
     * @type {Order}
     */
    order;

    status_colections_id = null;

    changeStatus = (value) => {
        this.status_colections_id = value;
    };

    setStatus = () => {
        if(this.status_colections_id === null) {
            return Alert.alert(t.status_not_selected);
        }
        const invoice_number = this.order.data[this.order.attributes.invoice_number.name];
        const api = new TTExpressRestApi(settings.token);

        api.orderSetStatus(invoice_number, this.status_colections_id)
            .then(data => {
                if(data.status === 'success'){
                    Alert.alert(data.response);
                    this.props.changeNumber(invoice_number);
                } else {
                    this.setState({
                        statusErrors: data.response
                    });
                }
            })
            .catch(err => console.error(err));
    };

    printSticker = () => {
        const invoice_number = this.order.data[this.order.attributes.invoice_number.name];
        const api = new TTExpressRestApi(settings.token);
        api.ordersSticker(invoice_number).catch(error => console.error(error));
    };

    componentWillMount() {
        this.order = new Order();
        this.order.setData(this.props.response);
    }

    render() {
        return (
            <>
                <Content style={{marginBottom: 50}}>
                    <Separator bordered>
                        <Text style={{fontSize: 25, textAlign: 'center'}}>{this.order.data[this.order.attributes.invoice_number.name]}</Text>
                    </Separator>

                    <ItemInfo
                        label={this.order.attributes.datetime.label}
                        value={this.order.data[this.order.attributes.datetime.name]}
                    />

                    <SenderView
                        model={this.order.getSender()}
                    />

                    <CarrierView
                        model={this.order.getCarrier()}
                    />

                    <CustomsPostView
                        model={this.order.getCustomsPost()}
                    />

                    <PersonView
                        model={this.order.getPerson()}
                    />

                    <Separator>
                        <Text style={styles.separatorText}>{t.actions}</Text>
                    </Separator>

                    {this.state.statusErrors !== null && <StatusErrors
                        errors={this.state.statusErrors}
                    />}

                    <StatusPicker onValueChange={this.changeStatus}/>

                    <Button onPress={this.printSticker} style={styles.btnThemeBorder}>
                        <Text style={{textAlign: 'center', width: '100%', color:color_theme}}>{t.print_sticker}</Text>
                    </Button>

                </Content>

                <View style={styles.footerSectionBtns}>
                    <Button full onPress={this.props.closeModal} style={{width: '50%', backgroundColor:color_theme}}>
                        <Text>{t.close}</Text>
                    </Button>
                    <Button full onPress={this.setStatus} style={[{width: '50%'}, styles.btnThemeBorder]}>
                        <Text style={{color:color_theme}}>{t.apply}</Text>
                    </Button>
                </View>
            </>
        );
    }
};
