import React from 'react';
import {List, ListItem, Text, Left, Body, Content, View} from 'native-base';
import t from '../language/Collections';
import {color_theme} from '../styles/Base.styles';
import ItemInfo from './ItemInfo';

const StatusErrors = (props) => {

    const errors = Object.entries(props.errors);

    console.log('errors', errors);

    return <>
        {errors.map((error, index) => {

            const attributes = Object.entries(error[1]);

            let msg = [];
            attributes.map((attribute)=>{
                attribute[1].map((text)=>{
                    msg.push(text);
                });
            });

            return (
                <ItemInfo
                    key={index}
                    isError={true}
                    label={error[0]}
                    value={msg.join('; ')}
                />
            )
        })}
    </>
};

export default StatusErrors;
