import React from 'react';
import {List, ListItem, Text, Left, Body, Separator, Content} from 'native-base';
import styles from '../styles/Base.styles';
import t from '../language/Collections';
import Carrier from '../models/Carrier';
import ItemInfo from './ItemInfo';
import PropTypes from 'prop-types';
import {Linking} from 'react-native';

const CarrierView = (props) => {
    return (
        <>
            <Separator>
                <Text style={styles.separatorText}>{t.carrier}</Text>
            </Separator>

            <ItemInfo
                label={props.model.attributes.name.label}
                value={props.model.data[props.model.attributes.name.name]}
            />

            <ItemInfo
                label={props.model.attributes.phone.label}
                onPress={()=>{
                    Linking.openURL(`tel:${props.model.data[props.model.attributes.phone.name]}`)
                }}
                value={props.model.data[props.model.attributes.phone.name]}
            />
        </>
    );
};

CarrierView.propTypes = {
    model: PropTypes.instanceOf(Carrier),
};

export default CarrierView;
