import React from 'react';
import {List, ListItem, Text, Left, Body, Content, View} from 'native-base';
import t from '../language/Collections';
import {color_theme} from '../styles/Base.styles';

const ListCorporationStatuses = (props) => {
    return <List>
        {props.statuses.map((status, index) => {
            return (
                <ListItem key={index}>
                    <Left style={{flexDirection:'column'}}>
                        <Text style={{width: '100%', textAlign: 'left'}}>{status.datetime}</Text>
                        {status.status_collections.covert &&
                            <Text style={{color:color_theme, width: '100%', textAlign: 'left'}}>{t.service}</Text>
                        }
                        <Text style={{width: '100%', textAlign: 'left'}}>{status.status_collections.scenario.join(', ')}</Text>
                    </Left>
                    <Body>
                        <Text>{status.status_collections.text}</Text>
                    </Body>
                </ListItem>
            )
        })}
    </List>
};

export default ListCorporationStatuses;
