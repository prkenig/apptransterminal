import React from 'react';
import {Spinner} from 'native-base';
import {color_theme} from '../styles/Base.styles.js';

const SpinnerTheme = (props) => {
    return (<Spinner color={color_theme} />)
};

export default SpinnerTheme;
