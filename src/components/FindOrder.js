import React from 'react';
import {Content, Input, Text, View, Button, Separator} from 'native-base';
import t from '../language/Collections';
import styles from '../styles/FindOrder.styles';

const FindOrder = (props) => {

    let input = React.createRef();

    const change = () => {
        let number = input._root._lastNativeText;
        props.onPress({data:number});
    };

    return <Content style={styles.content}>
        <View><Text style={styles.heading}>{t.find_heading}</Text></View>
        <Input ref={(ref) => {input = ref;}} style={styles.input}  placeholderTextColor={'white'} placeholder={t.find_placeholder}/>
        <Button onPress={change} rounded style={styles.button}><Text style={styles.button_text}>{t.find}</Text></Button>
    </Content>
};

export default FindOrder;
