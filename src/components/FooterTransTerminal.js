import React from 'react';
import { Text, View} from 'native-base';
import {color_theme} from '../styles/Base.styles.js';
import t from '../language/Collections';

const FooterTransTerminal = (props) => {
    return  <View padder style={{ backgroundColor: color_theme}}>
        <Text style={{color:'white', textAlign: 'center'}}>{t.name_company}</Text>
    </View>
};

export default FooterTransTerminal;
