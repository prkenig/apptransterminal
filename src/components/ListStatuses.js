import React from 'react';
import {List, ListItem, Text, Left, Body, Separator, Content} from 'native-base';

const ListStatuses = (props) => {
    return <List>
        {props.statuses.map((status, index) => {
            return (
                <ListItem key={index}>
                    <Left>
                        <Text>{status.datetime}</Text>
                    </Left>
                    <Body>
                        <Text>{status.status}</Text>
                    </Body>
                </ListItem>
            )
        })}
    </List>
};

export default ListStatuses;
