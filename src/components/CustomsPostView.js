import React from 'react';
import {List, ListItem, Text, Left, Body, Separator, Content} from 'native-base';
import styles from '../styles/Base.styles';
import t from '../language/Collections';
import CustomsPost from '../models/CustomsPost';
import ItemInfo from './ItemInfo';
import PropTypes from 'prop-types';

const CustomsPostView = (props) => {
    return (
        <>
            <Separator>
                <Text style={styles.separatorText}>{t.customs_post}</Text>
            </Separator>

            <ItemInfo
                label={props.model.attributes.name.label}
                value={props.model.data[props.model.attributes.name.name]}
            />
        </>
    );
};

CustomsPostView.propTypes = {
    model: PropTypes.instanceOf(CustomsPost),
};

export default CustomsPostView;
