import React from 'react';
import {List, ListItem, Text, Left, Body, Separator, Content} from 'native-base';
import styles from '../styles/Base.styles';
import t from '../language/Collections';
import Person from '../models/Person';
import ItemInfo from './ItemInfo';
import PropTypes from 'prop-types';
import {Linking} from 'react-native';

const PersonView = (props) => {
    return (
        <>
            <Separator>
                <Text style={styles.separatorText}>{t.person}</Text>
            </Separator>

            <ItemInfo
                label={props.model.attributes.name.label}
                value={props.model.data[props.model.attributes.name.name]}
            />

            <ItemInfo
                label={props.model.attributes.post.label}
                value={props.model.data[props.model.attributes.post.name]}
            />

            <ItemInfo
                label={props.model.attributes.phone.label}
                onPress={()=>{
                    Linking.openURL(`tel:${props.model.data[props.model.attributes.phone.name]}`)
                }}
                value={props.model.data[props.model.attributes.phone.name]}
            />
        </>
    );
};

PersonView.propTypes = {
    model: PropTypes.instanceOf(Person),
};

export default PersonView;
