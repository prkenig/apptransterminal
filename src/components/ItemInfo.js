import React from 'react';
import {Label, Content, View, Text} from 'native-base';

const ItemInfo = (props) => {
    return (
        <Content padder>
            <View>
                <Text style={{
                    fontSize: props.isError ? 13 : 10,
                    color: props.isError ? 'red' : 'gray'
                }}>{props.label}:</Text>
            </View>
            <Text onPress={props.onPress}>{props.value}</Text>
        </Content>
    );
};

ItemInfo.defaultProps = {
    onPress: ()=>{},
    isError: false
};

export default ItemInfo;
