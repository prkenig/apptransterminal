import t from '../language/Collections';
import BaseModel from './BaseModel';

export class Person extends BaseModel
{
    get attributes () {
        return {
            details: {
                name: 'details',
                label: t.details,
                value: null,
                type: String
            },
            name: {
                name: 'name',
                label: t.full_name,
                value: null,
                type: String
            },
            post: {
                name: 'post',
                label: t.post,
                value: null,
                type: String
            },
            phone: {
                name: 'phone',
                label: t.phone,
                value: null,
                type: String
            },
        }
    };
}

export const instance = new Person();

export default Person;
