import t from '../language/Collections';
import BaseModel from './BaseModel';

export class Order extends BaseModel
{
    get attributes () {
        return {
            city_en: {
                name: 'city_en',
                label: t.city_en,
                value: null,
                type: String
            },
            city_ru: {
                name: 'city_ru',
                label: t.city_ru,
                value: null,
                type: String
            },
            code: {
                name: 'code',
                label: t.code,
                value: null,
                type: String
            },
            name: {
                name: 'name',
                label: t.name,
                value: null,
                type: String
            },
        }
    };
}

export const instance = new Order();

export default Order;
