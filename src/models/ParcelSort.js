import t from '../language/Collections';
import BaseModel from './BaseModel';

export class ParcelSort extends BaseModel
{
    get attributes () {
        return {
            code: {
                name: 'code',
                label: t.code,
                value: null,
                type: String
            },
            name: {
                name: 'name',
                label: t.name,
                value: null,
                type: String
            },
        }
    };
}

export const instance = new ParcelSort();

export default ParcelSort;
