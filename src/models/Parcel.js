import t from '../language/Collections';
import BaseModel from './BaseModel';
import {Linking} from 'react-native';

export const KEY_WEIGHING = "weighing";
export const KEY_SIGNATURE = "signature";

export class Parcel extends BaseModel
{
    get attributes () {
        return {
            address: {
                name: 'address',
                label: t.address,
                value: null,
                type: String
            },
            city: {
                name: 'city',
                label: t.city,
                value: null,
                type: String
            },
            country: {
                name: 'country',
                label: t.country,
                value: null,
                type: String
            },
            description: {
                name: 'description',
                label: t.description,
                value: null,
                type: String
            },
            email: {
                name: 'email',
                label: t.email,
                value: null,
                type: String
            },
            full_name: {
                name: 'full_name',
                label: t.full_name,
                value: null,
                type: String
            },
            invoice_number: {
                name: 'invoice_number',
                label: t.invoice_number,
                value: null,
                type: String
            },
            link: {
                name: 'link',
                label: t.link,
                value: null,
                type: String
            },
            order_number: {
                name: 'order_number',
                label: t.order_number,
                value: null,
                type: String
            },
            package_height: {
                name: 'package_height',
                label: t.package_height,
                value: null,
                type: String
            },
            package_length: {
                name: 'package_length',
                label: t.package_length,
                value: null,
                type: String
            },
            package_weight: {
                name: 'package_weight',
                label: t.package_weight,
                value: null,
                type: String
            },
            package_width: {
                name: 'package_width',
                label: t.package_width,
                value: null,
                type: String
            },
            parcel_sorts: {
                name: 'parcel_sorts',
                label: t.parcel_sorts,
                value: null,
                type: String
            },
            sticker_consolidating: {
                name: 'sticker_consolidating',
                label: t.sticker_consolidating,
                value: null,
                type: String
            },
            phone: {
                name: 'phone',
                label: t.phone,
                value: null,
                type: String
            },
            postal_code: {
                name: 'postal_code',
                label: t.postal_code,
                value: null,
                type: String
            },
            quantity: {
                name: 'quantity',
                label: t.quantity,
                value: null,
                type: String
            },
            region: {
                name: 'region',
                label: t.region,
                value: null,
                type: String
            },
            status: {
                name: 'status',
                label: t.status,
                value: null,
                type: String
            },
            tracking_number: {
                name: 'tracking_number',
                label: t.tracking_number,
                value: null,
                type: String
            },
            unit_cost: {
                name: 'unit_cost',
                label: t.unit_cost,
                value: null,
                type: String
            },
            unit_weight: {
                name: 'unit_weight',
                label: t.unit_weight,
                value: null,
                type: String
            },
            signature: {
                name: 'signature',
                label: t.signature,
                value: null,
                type: String
            },
        }
    };

    addressFormat(){
        return [
            this.data.postal_code,
            this.data.country,
            this.data.region,
            this.data.city,
            this.data.address,
        ].join(', ');
    }

    getCurrentKey(){
        let statuses = this.data[this.attributes.status.name];

        if(statuses.length === 0) return null;

        let lastStatus = statuses[statuses.length-1];

        return lastStatus.status_collections.key;
    }

    getTrackingNumber(){
        return this.data[this.attributes.tracking_number.name];
    }

    isEmptyWeight(){
        const package_weight = this.data[this.attributes.package_weight.name];
        return package_weight === null || typeof package_weight === 'undefined';
    }

    isEmptySignature(){
        const signature = this.data[this.attributes.signature.name];
        return signature === null || typeof signature === 'undefined';
    }
}

export const instance = new Parcel();

export default Parcel;
