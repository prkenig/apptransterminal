import AsyncStorage from "@react-native-community/async-storage";

export default class BaseModel
{
    get attributes () {
        return {
            name_attribute: {
                name: 'name',
                label: 'label',
                value: 'value',
                type: Number
            },
        }
    };

    data = {};

    constructor() {
        Object.values(this.attributes).map((object) => {
            this.data[object.name] = object.value;
        });
    }

    setData = (data) => {
        data = this.beforeSetData(data);

        const newData = Object.assign({}, this.data, data);

        Object.values(this.attributes).map((object) => {
            this.data[object.name] = (newData[object.name] !== undefined) ? newData[object.name] : object.value;
        });
    };

    beforeSetData = (data) => {return data};

    beforeSave = () => {};

    afterSave = async () => {
        return await true;
    };

    save = () => {
        this.beforeSave();
        this.afterSave().catch(err => { console.error(err) });
    };
}