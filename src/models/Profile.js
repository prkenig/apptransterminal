import t from '../language/Collections';
import LocalStorageModel from './LocalStorageModel';
import DeviceInfo from "react-native-device-info";
import {db, firestore} from '../config/db';
import { uploadImage } from '../helpers/HelperImages';
import { YellowBox} from 'react-native';
import {Toast} from 'native-base';
import ImageResizer from 'react-native-image-resizer';
import {flags} from "../components/flags";

YellowBox.ignoreWarnings(['Setting a timer']);

const REF_PROFILES = 'profiles';

export class Profile extends LocalStorageModel
{
    VALUE_RIGHT_HANDED_TWO_HANDED_BACKHAND = 'right_handed_two_handed_backhand';
    VALUE_LEFT_HANDED_TWO_HANDED_BACKHAND = 'left_handed_two_handed_backhand';
    VALUE_RIGHT_HANDED_ONE_HANDED_BACKHAND = 'right_handed_one_handed_backhand';
    VALUE_LEFT_HANDED_ONE_HANDED_BACKHAND = 'left_handed_one_handed_backhand';

    get KEY_MODEL() {
        return "profile";
    }

    get attributes () {
        return {
            id: {
                name: 'id',
                label: 'id',
                value: DeviceInfo.getUniqueID(),
                type: Number
            },
            name: {
                name: 'name',
                label: t.name,
                value: null,
                type: String
            },
            avatar: {
                name: 'avatar',
                label: t.avatar,
                value: null,
                type: String
            },
            avatar_remote: {
                name: 'avatar_remote',
                label: t.avatar,
                value: null,
                type: String
            },
            avatar_min: {
                name: 'avatar_min',
                label: t.avatar,
                value: null,
                type: String
            },
            // birthplace: {
            //     name: 'birthplace',
            //     label: t.birthplace,
            //     value: ['US'],
            //     type: String
            // },
            residence: {
                name: 'residence',
                label: t.residence,
                value: ['US'],
                type: String
            },
            date_of_birth: {
                name: 'date_of_birth',
                label: t.date_of_birth,
                value: null,
                type: String
            },
            plays: {
                name: 'plays',
                label: t.plays,
                value: '',
                type: String
            },
            weight: {
                name: 'weight',
                label: t.weight,
                value: null,
                type: String
            },
            height: {
                name: 'height',
                label: t.height,
                value: null,
                type: String
            },
            license: {
                name: 'license',
                label: t.license,
                value: null,
                type: String
            },
            email: {
                name: 'email',
                label: t.email,
                value: null,
                type: String
            },
            atp_ranking: {
                name: 'atp_ranking',
                label: t.atp_ranking,
                value: null,
                type: String
            },
            itf_ranking: {
                name: 'itf_ranking',
                label: t.itf_ranking,
                value: null,
                type: String
            },
            national_ranking: {
                name: 'national_ranking',
                label: t.national_ranking,
                value: null,
                type: String
            },
            medical_assessment: {
                name: 'medical_assessment',
                label: t.medical_assessment,
                value: null,
                type: String
            },
            fisio_visit: {
                name: 'fisio_visit',
                label: t.fisio_visit,
                value: null,
                type: String
            },
            medical_notes: {
                name: 'medical_notes',
                label: t.medical_notes,
                value: null,
                type: String
            },
        }
    };

    allModels = (ids = null) => {
        let profilesRef = db.ref('/'+REF_PROFILES);

        return new Promise((resolve, reject) => {
            profilesRef.once('value')
                .then((snapshot) => {
                    let objects = snapshot.val();
                    let items = Object.values(objects);
                    let collections = [];

                    items.map((object) => {

                        if(object.id === this.data.id && ids === null) return true;

                        if(ids !== null && !ids.includes(object.id)) return true;

                        let model = new Profile();
                        model.setData(object);
                        collections.push(model);
                    });

                    resolve(collections);
                })
                .catch((error) => {reject(error)});
        });
    };

    findById = (id) => {
        let ref = db.ref('/'+REF_PROFILES+'/'+id);

        return new Promise((resolve, reject) => {
            ref.once('value')
                .then((snapshot) => {
                    let object = snapshot.val();

                    let model = new Profile();
                    model.setData(object);

                    resolve(model);
                })
                .catch((error) => {reject(error)});
        });
    };

    flag = () => {
        return flags[this.data.residence[0]]
    };

    avatarBig = (self = false) => {
        const uriNoPhoto = require("../images/startPage2.jpg");
        const uriAvatar =  self ? this.data.avatar : this.data.avatar_remote;

        return uriAvatar === null || uriAvatar === undefined ? uriNoPhoto : { uri: uriAvatar };
    };

    avatarMin = () => {
        const uriNoPhoto = require("../images/no-photo.png");
        const uriAvatar =  this.data.avatar_min;

        return uriAvatar === null || uriAvatar === undefined ? uriNoPhoto : { uri: uriAvatar };
    };

    afterSave = async () => {
        this.remoteSave();
        return await true;
    };

    remoteSave = () => {

        //firestore
        firestore.runTransaction(async transaction => {
            transaction.set(firestore.collection(REF_PROFILES).doc(this.data.id), this.data);
        }).catch(error => { console.error('Transaction failed: ', error); });

        //firebase
        db.ref('/'+REF_PROFILES+'/'+this.data.id)
            .set(this.data)
            .then(()=>{
                Toast.show({
                    text: t.success_save,
                    buttonText: t.okay,
                    type: "success",
                    position: "top"
                });
            })
            .catch(err => { console.error(err) });
    };

    saveImage = (response, callback) => {
        this.setData({avatar:response.uri});

        this.save().then((res)=>{
            callback();
        }).catch(err => { console.error(err) });

        uploadImage(response.uri, 'images/'+this.data.id)
            .then(url => {
                this.setData({avatar_remote:url});
                this.save().catch(err => { console.error(err) });
            })
            .catch(error => console.error(error));

        ImageResizer.createResizedImage(response.uri, 200, 200, 'JPEG', 90)
            .then((result) => {
                uploadImage(result.uri, 'images/'+this.data.id)
                    .then(url => {
                        this.setData({avatar_min:url});
                        this.save().catch(err => { console.error(err) });
                    })
                    .catch(error => console.error(error));
            }).catch((error) => console.error(error));
    };
}

export const instance = new Profile();

export default Profile;