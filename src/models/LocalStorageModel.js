import AsyncStorage from "@react-native-community/async-storage";
import BaseModel from "./BaseModel";

export default class LocalStorageModel extends BaseModel
{
    get KEY_MODEL() {
        return "base_model";
    };

    save = () => {
        this.beforeSave();
        return new Promise((resolve, reject) => {
            AsyncStorage.setItem(this.KEY_MODEL, JSON.stringify(this.data))
                .then(() => {
                    this.afterSave();
                    resolve(true);
                })
                .catch(err => reject(err));
        });
    };

    reset = () => {
        return new Promise((resolve, reject) => {
            AsyncStorage.removeItem(this.KEY_MODEL)
                .then(() => {
                    resolve(true);
                })
                .catch(err => reject(err));
        });
    };

    load = () => {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(this.KEY_MODEL)
                .then(res => {
                    if (res === null) {
                        resolve(false);
                    } else {
                        this.data = JSON.parse(res);
                        resolve(true);
                    }
                })
                .catch(err => reject(err));
        });
    };
}