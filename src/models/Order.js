import t from '../language/Collections';
import BaseModel from './BaseModel';
import Sender from './Sender';
import Carrier from './Carrier';
import CustomsPost from './CustomsPost';
import Person from './Person';

export class Order extends BaseModel
{
    get attributes () {
        return {
            datetime: {
                name: 'datetime',
                label: t.datetime,
                value: null,
                type: String
            },
            sender: {
                name: 'sender',
                label: t.sender,
                value: null,
                type: String
            },
            carrier: {
                name: 'carrier',
                label: t.carrier,
                value: null,
                type: String
            },
            invoice_number: {
                name: 'invoice_number',
                label: t.invoice_number,
                value: null,
                type: String
            },
            direction: {
                name: 'direction',
                label: t.direction,
                value: null,
                type: String
            },
            customs_post: {
                name: 'customs_post',
                label: t.customs_post,
                value: null,
                type: String
            },
            person: {
                name: 'person',
                label: t.person,
                value: null,
                type: String
            }
        }
    };

    getPerson(){
        let model = new Person();

        if(this.data.person !== null){
            model.setData(this.data.person);
        }

        return model;
    }

    getSender(){
        let model = new Sender();

        if(this.data.sender !== null){
            model.setData(this.data.sender);
        }

        return model;
    }

    getCarrier(){
        let model = new Carrier();

        if(this.data.carrier !== null){
            model.setData(this.data.carrier);
        }

        return model;
    }

    getCustomsPost(){
        let model = new CustomsPost();

        if(this.data.customs_post !== null){
            model.setData(this.data.customs_post);
        }

        return model;
    }
}

export const instance = new Order();

export default Order;
