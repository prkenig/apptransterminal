import t from '../language/Collections';
import BaseModel from './BaseModel';

export class Sender extends BaseModel
{
    get attributes () {
        return {
            address: {
                name: 'address',
                label: t.address,
                value: null,
                type: String
            },
            city: {
                name: 'city',
                label: t.city,
                value: null,
                type: String
            },
            country: {
                name: 'country',
                label: t.country,
                value: null,
                type: String
            },
            logo_url: {
                name: 'logo_url',
                label: t.logo_url,
                value: null,
                type: String
            },
            name: {
                name: 'name',
                label: t.name,
                value: null,
                type: String
            },
            number_of_contract: {
                name: 'number_of_contract',
                label: t.number_of_contract,
                value: null,
                type: String
            },
            currency: {
                name: 'currency',
                label: t.currency,
                value: null,
                type: String
            },
            phone: {
                name: 'phone',
                label: t.phone,
                value: null,
                type: String
            }
        }
    };
}

export const instance = new Sender();

export default Sender;
