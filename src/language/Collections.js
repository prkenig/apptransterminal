import React from 'react';
import LocalizedStrings from 'react-native-localization';
import {strings as ru} from './ru';
import {strings as en} from './en';
import {strings as ch} from './ch';

const t = new LocalizedStrings({
    en:en,
    ru:ru,
    ch:ch
});

export default t;
