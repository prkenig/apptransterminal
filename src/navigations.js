import {NavigationActions, StackActions} from 'react-navigation';
import React from 'react';
import HomeScreen from "./screens/HomeScreen";
import {setSettings} from './settnigs';

let _navigator;

export function setTopLevelNavigator(navigatorRef) {
    _navigator = navigatorRef;
}

export function navigate(routeName, params) {
    _navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        })
    );
};

export function restart() {
    let nav = _navigator.state.nav;
    let routes = nav.routes;
    let index = nav.index;
    let actions = [];

    routes.map((value, i) => {
        actions.push(NavigationActions.navigate({ routeName: value.routeName }));
    });

    _navigator.dispatch(
        StackActions.reset({
            index: index,
            actions: actions,
        })
    );
};

export function goToHome() {
    _navigator.dispatch(
        StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'HomeScreen' })],
        })
    );
};


export async function logout() {
    await setSettings({token: null}).then(res => {
        navigate('Auth');
    });
};
