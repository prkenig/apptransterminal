import { createStore, combineReducers } from 'redux';
import { reducer as formReducer  } from 'redux-form';

const rootReducer = combineReducers({
    form: formReducer
});

const configureStore = () => {
    return createStore(rootReducer);
};

export default configureStore;
