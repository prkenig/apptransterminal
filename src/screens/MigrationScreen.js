import React, { Component } from 'react';
import {Button, Container, Content, Icon, Text, List, ListItem} from 'native-base';


export default class MigrationScreen extends Component {

    /**
     * Migrations list
     * @type {(function(): (Promise<any> | Promise<*>))[]}
     */
    migrations = [
        //migrate_groups,
        //migrate_profiles,
    ];

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'Migrations'),
        };
    };

    state = {
        migrationsEnded: [],
        migrationsError: [],
    };

    success = (name) => {
        this.setState({
            migrationsEnded: [...this.state.migrationsEnded, name]
        });
    };

    error = (name) => {
        this.setState({
            migrationsError: [...this.state.migrationsError, name]
        });
    };

    onStart = () => {
        this.migrations.map((func) => {
            func().then(res => {
                    this.success(func.name);
                }) .catch(error => {
                    this.error(func.name);
                });
        });
    };

    render() {

        const MigrationList = () => {
          return (
              <List>
                  {this.state.migrationsEnded.map((name, index) => {
                      return <ListItem key={index}>
                          <Text>{name}</Text>
                      </ListItem>
                  })}
                  {this.state.migrationsError.map((name, index) => {
                      return <ListItem key={index}>
                          <Text style={{color: 'darkred'}}>{name}</Text>
                      </ListItem>
                  })}
                  <ListItem itemDivider>
                      <Text>All migration: {this.migrations.length} / Errors: {this.state.migrationsError.length} / Success: {this.state.migrationsEnded.length}</Text>
                  </ListItem>
              </List>
          );
        };

        return (
            <Container>
                <Content padder>
                    { this.migrations.length === 0 ? <Text>Migrations not found</Text> : <MigrationList/> }
                </Content>
                <Button iconLeft full light onPress={this.onStart}>
                    <Icon name='ios-play' />
                    <Text>{'Start'}</Text>
                </Button>
            </Container>
        );
    }
}
