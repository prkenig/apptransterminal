import React, { Component } from 'react';
import { KeyboardAvoidingView } from 'react-native';
import {
    Container,
    Text,
    Icon,
    Button,
    Content,
    Header,
    H1,
    Left,
    Drawer,
    Body,
    Title,
    Right,
    H3,
    Separator,
} from 'native-base';
import {Dimensions, Image, ImageBackground, StyleSheet, TouchableOpacity, Linking} from 'react-native';
import t from '../language/Collections';
import {color_theme} from '../styles/Base.styles';
import SideBar from '../layouts/SideBar';
import {settings} from '../settnigs';
import SearchContent from '../components/SearchContent';
import FindOrder from '../components/FindOrder';
import QRCodeScanner from 'react-native-qrcode-scanner';
import Modal from "react-native-modal";
import TTExpressRestApi from '../client/TTExpressRestApi';

const { width, height } = Dimensions.get('window');

console.disableYellowBox = true;

export class HomeScreen extends Component {

    state = {
        isModalVisible: false,
        number: '',
        roles: [],
    };

    openModal = () => {
        this.setState({isModalVisible: true});
    };

    closeModal = () => {
        this.setState({isModalVisible: false});
        if(typeof this.scanner !== 'undefined') this.scanner.reactivate();
    };

    closeDrawer = () => {
        this.drawer._root.close();
    };

    openDrawer = () => {
        this.drawer._root.open();

        const api = new TTExpressRestApi(settings.token);
        api.roles()
            .then(data => {
                if(data.status === 'success'){
                    this.setState({roles: Object.values(data.response.roles)});
                }
            })
            .catch(err => console.error(err));
    };

    onSuccess = (e) => {
        let number = e.data;
        this.setState({number: number});
        this.openModal();
    };

    render() {
        return (
            <Drawer
                ref={(ref) => {this.drawer = ref;}}
                content={<SideBar roles={this.state.roles}/>}
                onClose={() => this.closeDrawer()}
            >
                <Container>
                    <ImageBackground source={require('../images/background_2.jpg')} style={{width: width, height: height}}>
                        <Header androidStatusBarColor={color_theme} style={{backgroundColor:color_theme}}>
                            <Left>
                                <Button transparent onPress={this.openDrawer}>
                                    <Icon name='menu' />
                                </Button>
                            </Left>
                            <Body>
                                <Title>{t.scanner}</Title>
                            </Body>
                            <Right/>
                        </Header>
                        <Content>
                            <KeyboardAvoidingView behavior="position" enabled>

                                    {!__DEV__ && <QRCodeScanner
                                        ref={(node) => { this.scanner = node }}
                                        onRead={this.onSuccess}
                                        fadeIn={false}
                                        showMarker={true}
                                    />}

                                    <FindOrder onPress={this.onSuccess}/>

                            </KeyboardAvoidingView>
                        </Content>
                    </ImageBackground>
                    <Modal
                        isVisible={this.state.isModalVisible}
                        onBackdropPress={this.closeModal}
                    >
                        <Container style={{backgroundColor: 'white'}}>
                            <SearchContent
                                number={this.state.number}
                                closeModal={this.closeModal}
                            />
                        </Container>
                    </Modal>
                </Container>
            </Drawer>
        );
    }
}

export default HomeScreen;
