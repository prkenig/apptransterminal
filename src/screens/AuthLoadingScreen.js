import React from 'react';
import { Content, Grid, Col, Text, View} from 'native-base';
import SpinnerTheme from '../components/SpinnerTheme';
import t from '../language/Collections';
import {color_theme} from '../styles/Base.styles';
import { getSettings } from "../settnigs";
import { navigate } from "../navigations";
import FooterTransTerminal from '../components/FooterTransTerminal';

export default class AuthLoadingScreen extends React.Component {

    constructor(props) {
        super(props);
        this._bootstrapAsync().catch(error => console.error(error));
    }

    _bootstrapAsync = async () => {
        const settings = await getSettings();
        if(settings.language !== null) t.setLanguage(settings.language);
        navigate(settings.token ? 'App' : 'Auth');
    };

    render() {
        return (
            <Content contentContainerStyle={{flex: 1}}>
                <Grid style={{alignItems: 'center'}}>
                    <Col>
                        <SpinnerTheme />
                    </Col>
                </Grid>
                <FooterTransTerminal/>
            </Content>
        );
    }
}
