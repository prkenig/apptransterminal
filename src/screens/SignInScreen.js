import React from 'react';
import { color_theme } from '../styles/Base.styles';
import {
    Container,
    Button,
    Text,
    Form,
    Item as FormItem,
    Input,
    Label,
    Content, View,
} from 'native-base';
import {ImageBackground, Dimensions, Image} from 'react-native';
import t from '../language/Collections';
import TTExpressRestApi from '../client/TTExpressRestApi';
import { navigate } from "../navigations";
import { setSettings } from "../settnigs";
import ListStatuses from '../components/ListStatuses';
import SpinnerTheme from '../components/SpinnerTheme';
import AutoHeightImage from 'react-native-auto-height-image';

const { width, height } = Dimensions.get('window');

export default class SignInScreen extends React.Component {

    constructor(props) {
        super(props);

        this.email_input = React.createRef();
        this.password_input = React.createRef();
    }

    state = {
        error_message: '',
        email_error: false,
        password_error: false,
        loading: false,
    };

    // _signInAsync = async () => {
    //     await AsyncStorage.setItem('userToken', 'abc');
    //     this.props.navigation.navigate('App');
    // };

    changeForm = () => {
        let email_value = this.email_input.props.value;
        let password_value = this.password_input.props.value;

        let error_message = '';

        if(email_value === ''){
            this.setState({email_error: true});
            setTimeout(()=>{
                this.setState({email_error: false});
                this.setState({error_message: ''})
                }, 1500);
            error_message = t.input_error;
        }

        if(password_value === ''){
            this.setState({password_error: true});
            setTimeout(()=>{
                this.setState({password_error: false});
                this.setState({error_message: ''})
                }, 1500);
            error_message = t.input_error;
        }

        if(error_message !== '') {
            this.setState({error_message: error_message});
            return false;
        }

        this.setState({loading: true});

        const api = new TTExpressRestApi();

        api.login(email_value, password_value)
            .then(data => {
                if(data.status === 'success'){
                    this.saveToken(data.response.token).catch(error => console.error(error));
                } else {
                    this.setState({error_message: data.response})
                }
                this.setState({loading: false});
            })
            .catch(err => console.error(err.message));
    };

    saveToken = async (token) => {
        await setSettings({token: token}).then(res => {
            navigate('App');
        });
    };

    render() {
        return (
            <Container>
                <ImageBackground source={require('../images/background.jpg')} style={{width: width, height: height}}>
                    <Content padder>
                        <Form>
                            <View style={{flexDirection: "row", justifyContent: "center", marginTop: 30}}>
                                <Image source={require('../images/logo_transterminal.png')} style={{width: 300, height:69, marginBottom:30}}/>
                            </View>
                            <FormItem floatingLabel error = {this.state.email_error}>
                                <Label>{t.email}</Label>
                                <Input getRef={ref => this.email_input = ref} />
                            </FormItem>
                            <FormItem floatingLabel error = {this.state.password_error}>
                                <Label>{t.password}</Label>
                                <Input getRef={ref => this.password_input = ref} secureTextEntry={true} />
                            </FormItem>

                            {this.state.error_message !== '' && <View padder><Text style={{color:'darkred', marginLeft: 5}}>{this.state.error_message}</Text></View>}

                            { this.state.loading && <SpinnerTheme/> }

                            <Button onPress={this.changeForm} rounded style={{marginTop: 40, backgroundColor:color_theme, justifyContent: "center", alignSelf: "stretch", textAlignVertical: "center" }}>
                                <Text style={{width:'100%', textAlign:'center'}}>{t.login}</Text>
                            </Button>

                            <View style={{justifyContent: 'center', alignItems: 'center' }}>
                                <AutoHeightImage
                                    width={width}
                                    style={{marginTop: 30, marginBottom: 30}}
                                    source={require('../images/track.png')}
                                />
                            </View>
                        </Form>
                    </Content>
                </ImageBackground>
            </Container>
        );
    }
}
