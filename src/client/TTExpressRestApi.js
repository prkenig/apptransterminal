import RestClient from 'react-native-rest-client';
import {logout} from '../navigations';
import {Linking} from 'react-native';
import RNPrint from 'react-native-print';

export default class TTExpressRestApi extends RestClient {

    authToken;

    constructor (authToken = null) {
        let params = {};

        if(__DEV__) {
            params['devMode'] = true;
            params['simulatedDelay'] = 1500;
        }

        if(authToken !== null) {
            params['headers'] = {
                Authorization: `Bearer ${authToken}`
            }
        }

        super('https://tt-express.ru/api', params);

        this.authToken = authToken;
    }

    login (username, password) {
        return this.POST('/users/login', {username:username, password:password});
    }

    status (number) {
        return this.GET('/statuses/'+number, {});
    }

    roles () {
        return this.GET('/users/roles', {}).then(this.hasAuth);
    }

    parcelsView (number) {
        return this.GET('/parcels/view/'+number, {}).then(this.hasAuth);
    }

    parcelsSetStatus (number, status_collections_id) {
        return this.POST('/parcels/set-status/'+number, {status_collections_id: status_collections_id}).then(this.hasAuth);
    }

    parcelsSetWeight (number, data_package) {
        return this.POST('/parcels/set-weight/'+number, data_package).then(this.hasAuth);
    }

    parcelsSetSignature (number, signature) {
        return this.POST('/parcels/set-signature/'+number, {signature:signature}).then(this.hasAuth);
    }

    async parcelsSticker (number) {
        let url = this._fullRoute('/parcels/sticker/'+number+`?token=${this.authToken}`);
        await RNPrint.print({ filePath: url })
    }

    async ordersSticker (number) {
        let url = this._fullRoute('/orders/sticker/'+number+`?token=${this.authToken}`);
        await RNPrint.print({ filePath: url })
    }

    async parcelSortsSticker (number) {
        let url = this._fullRoute('/parcel-sorts/sticker/'+number+`?token=${this.authToken}`);
        await RNPrint.print({ filePath: url })
    }

    parcelSortSetStatus (number, status_collections_id) {
        return this.POST('/parcel-sorts/set-status/'+number, {status_collections_id: status_collections_id}).then(this.hasAuth);
    }

    orderSetStatus (number, status_collections_id) {
        return this.POST('/orders/set-status/'+number, {status_collections_id: status_collections_id}).then(this.hasAuth);
    }

    ordersView (number) {
        return this.GET('/orders/view/'+number, {}).then(this.hasAuth);
    }

    parcelSortsView (number) {
        return this.GET('/parcel-sorts/view/'+number, {}).then(this.hasAuth);
    }

    statusCollections () {
        return this.GET('/users/status-collections', {}).then(this.hasAuth);
    }

    hasAuth (response) {
        if(typeof response.status !== 'undefined' && response.status === 401) {
            logout().catch(error => console.error(error));
            return false;
        }

        return response;
    }
};
