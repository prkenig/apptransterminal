import { StyleSheet } from 'react-native';

export let color_theme = '#ff9100';

export default StyleSheet.create({
    separatorText:{
        fontSize: 13
    },
    footerSectionBtns: {
        flex:1,
        flexDirection: 'row',
        position: 'absolute',
        bottom:0,
        justifyContent: 'flex-end'
    },
    btnThemeBorder:{
        backgroundColor:'white',
        borderWidth:2,
        borderColor: color_theme
    }
});
