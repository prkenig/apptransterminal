import { StyleSheet } from 'react-native';
import { color_theme } from './Base.styles';

export default StyleSheet.create({
    content:{
        backgroundColor:color_theme,
        padding: 20,
        margin: 20,
        shadowColor: '#1c2130',
        shadowOffset: { width: 0, height: 10 },
        shadowRadius: 2,
        elevation: 5,
    },
    heading:{
        color:'white',
        textAlign: 'center',
        fontSize: 25,
        marginBottom: 30
    },
    input:{
        color: 'white',
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 50,
        marginBottom: 30,
        paddingLeft: 15,
        paddingRight: 15,
    },
    button:{
        backgroundColor: 'white'
    },
    button_text:{
        width: '100%',
        textAlign: 'center',
        color: color_theme
    }
});
