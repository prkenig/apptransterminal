import React from 'react';
import {color_theme} from '../styles/Base.styles.js';
import {
    View,
    ListItem,
    Button,
    Icon,
    Text,
    Container,
    Header,
    Left,
    Body,
    Switch,
    Right,
    Content,
    H1, Separator,
} from 'native-base';
import t from '../language/Collections';
import {ImageBackground} from 'react-native';
import AutoHeightImage from "react-native-auto-height-image";
import LanguageButton from '../components/buttons/LanguageButton';
import {navigate, logout} from '../navigations';
import SpinnerTheme from '../components/SpinnerTheme';
import styles from '../styles/Base.styles';

const SideBar = (props) => {
    return (
        <Container style={{backgroundColor:'white'}}>
            <ImageBackground source={require('../images/background.jpg')} style={{width: '100%', height: 100}}>
                <View style={{justifyContent: 'center', alignItems: 'center' }}>
                    <AutoHeightImage
                        width={200}
                        style={{marginTop: 30, marginBottom: 30}}
                        source={require('../images/logo_transterminal.png')}
                    />
                </View>
            </ImageBackground>
            <Content>
                <Separator bordered>
                    <Text style={styles.separatorText}>{t.roles}</Text>
                </Separator>

                {props.roles.length === 0 && <SpinnerTheme/>}

                {props.roles.map((role, index) => {
                    return (
                        <ListItem key={index}>
                            <Text>{role}</Text>
                        </ListItem>
                    )
                })}

                <Separator bordered>
                    <Text style={styles.separatorText}>{t.settingsHeading}</Text>
                </Separator>
                <ListItem icon>
                    <Left>
                        <Button style={{ backgroundColor: color_theme }}>
                            <Icon active name="md-globe" />
                        </Button>
                    </Left>
                    <Body>
                        <Text>{t.language}</Text>
                    </Body>
                    <Right>
                        <LanguageButton/>
                    </Right>
                </ListItem>
                <Separator bordered>
                    <Text style={styles.separatorText}>{t.other_actions}</Text>
                </Separator>
                <ListItem icon onPress={() => {logout().catch(error => console.error(error))}}>
                    <Left>
                        <Button style={{ backgroundColor: color_theme }}>
                            <Icon active name="ios-log-out" />
                        </Button>
                    </Left>
                    <Body>
                        <Text>{t.log_out}</Text>
                    </Body>
                    <Right>
                    </Right>
                </ListItem>

                {__DEV__ && <Separator bordered>
                    <Text style={styles.separatorText}>{t.debug}</Text>
                </Separator>}

                {__DEV__ && <Button iconLeft full light onPress={() => navigate('MigrationScreen')}>
                    <Icon name='ios-apps' />
                    <Text>{'Migrations'}</Text>
                </Button>}

            </Content>
        </Container>
    )
};

export default SideBar;
