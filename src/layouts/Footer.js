import React, { Component } from 'react';
import { Footer, FooterTab, Button, Icon, Text, Badge } from 'native-base';
import t from '../language/Collections';
import {navigate, reset} from "../router";

export default class FooterLayout extends Component {
    render() {
        return (
            <Footer >
                <FooterTab style={{backgroundColor: '#243773'}}>
                    <Button onPress={() => navigate('CalendarScreen')}>
                        <Icon name="calendar" />
                    </Button>
                    <Button onPress={() => navigate('GroupListScreen')}>
                        <Icon name="md-tennisball" />
                    </Button>
                    <Button onPress={() => navigate('UsersScreen')}>
                        <Icon name="md-people" />
                    </Button>
                    <Button onPress={() => navigate('SettingsScreen')}>
                        <Icon name="settings" />
                    </Button>
                </FooterTab>
            </Footer>
        );
    }
}