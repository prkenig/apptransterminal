import React from 'react';
import { createAppContainer } from "react-navigation";
import { switchNavigator } from "./src/router";
import { setTopLevelNavigator } from "./src/navigations";
import {Root} from "native-base";

export default class App extends React.Component {
  render() {
    const AppNavigator = createAppContainer(switchNavigator);

    return <Root>
      <AppNavigator ref={ref => setTopLevelNavigator(ref)} />
    </Root>
  }
}
